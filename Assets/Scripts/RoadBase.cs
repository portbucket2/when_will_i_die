using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadBase : MonoBehaviour
{
    LevelBase level;
    bool alreadyturned;
    // Start is called before the first frame update
    void Start()
    {
        level = GetComponentInParent<LevelBase>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void enteredroad()
    {
        if (!alreadyturned)
        {
            alreadyturned = true;
            level.turn(transform);
        }
    }
}
