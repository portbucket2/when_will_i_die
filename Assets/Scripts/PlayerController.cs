using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    Transform camerapos;
    public Transform[] camerapositions;
    int camposindex;
    public Vector2 cameralerprates=Vector2.one;
    Vector3 offset;
    Vector3 initclickpos;
    float inputlimit;
    float inputlimitcenter;
    public float inputlimitmax = 10;
    public AnimationCurve endanim;
    public float inputscale = 10;
    public designinterface inputscaleinterface;
    public float inputdragtoomuch = 10;
    public float yrot;
    public float ydelrot;
    public static PlayerController instance;
    float timeslowed;
    bool transitionedfromTaptoHold;
    public bool shouldmove;
    Vector3 initpos;
    Quaternion initrot;
    [Range(0, 1)] public float slowdownfraction = 0.1f;
    public bool gameover;
    float movevalue;
    float mainvalue;
    public System.Action diedingame, gamend, gamefinished;
    public bool alwaysmove
    {
        get
        {
            return PlayerPrefs.GetInt("TOGGLE_MOVE_STATUS", 1) != 0;
        }

        set
        {
            PlayerPrefs.SetInt("TOGGLE_MOVE_STATUS", value ? 1 : 0);
        }
    }

    public float coinscore
    {
        get
        {
            return PlayerPrefs.GetFloat("COIN", 0) ;
        }

        set
        {
            PlayerPrefs.SetFloat("COIN", value);
        }
    }

    float agescore;

    float timer;

    public float tempspeed;

    public float endistscale = 10;

    public UnityEngine.UI.Toggle automovetoggle;
    public UnityEngine.UI.Button restart;
    public UnityEngine.UI.Text scoretext;
    public UnityEngine.UI.Text timertext;
    public TMPro.TMP_Text multipliertext;
    public TMPro.TMP_Text cointext;
    //public UnityEngine.UI.Text feedbacktext;
    public TMPro.TMP_Text feedbacktext;
    public Color positivefdback = Color.white;
    public Color negativefdback = Color.red;
    public float updist = 2;
    public float uptime = 2;
    Color initcolorfb;
    Vector2 initposfb;

    public ParticleSystem dressparticle;
    public ParticleSystem maleparticle;
    public ParticleSystem bloodparticle;
    public Animator[] animators;
    public GameObject neutral, male, female;
    public float highscoretracker;
    public float currentcoin;
    public float currentmult;

    public Transform coinhome;
    public Transform coinstart;
    public GameObject coin;

    private void Awake()
    {
        instance = this;
        inputlimit = inputlimitmax;
    }
    // Start is called before the first frame update
    void Start()
    {
        foreach (Animator anim in animators)
        {
            NormalisedTimeCallback ntc = anim.GetBehaviour<NormalisedTimeCallback>();
            if (ntc) ntc.timelines[0].callback.AddListener(bloodparticle.Play);
        }
        coinctrl(0);
        initcolorfb = feedbacktext.color;
        initposfb = feedbacktext.rectTransform.anchoredPosition;
        camerapos = Camera.main.transform;
        restart.onClick.AddListener(() => UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex));
        //automovetoggle.isOn = alwaysmove;
        //automovetoggle.onValueChanged.AddListener((val) => alwaysmove = val);
        if(alwaysmove)shouldmove = true;

        movevalue = transform.position.x;
    }
    // Update is called once per frame
    void Update()
    {
        camerapos.transform.position = Vector3.Lerp(camerapos.transform.position, camerapositions[camposindex].position, Time.deltaTime * cameralerprates.x);
        camerapos.transform.rotation = Quaternion.Lerp(camerapos.transform.rotation, camerapositions[camposindex].rotation, Time.deltaTime * cameralerprates.y);
        if (Input.GetMouseButtonDown(0))
        {
            initpos = transform.position;
            initclickpos = Input.mousePosition;
            yrot = 0;
            timeslowed = 0;
            if (!alwaysmove) shouldmove = true;
            //player.startmov();
        }
        else if (Input.GetMouseButton(0))
        {
            ydelrot = (Input.mousePosition.x - (yrot + initclickpos.x)) / Screen.width;
            yrot = (Input.mousePosition - initclickpos).x;

            movevalue = transform.position.x + ydelrot * inputscale; //inputscaleinterface.designvalue;

            if (!gameover)
            {
                transform.position = Vector3.right * movevalue;

                if (movevalue < inputlimitcenter-inputlimit)
                {
                    transform.position = Vector3.right * (inputlimitcenter - inputlimit);
                }
                else if (movevalue > inputlimitcenter+inputlimit)
                {
                    transform.position = Vector3.right * (inputlimitcenter+inputlimit);
                }
            }

        }
        else if (Input.GetMouseButtonUp(0))
        {
            if (transitionedfromTaptoHold)
            {
                transitionedfromTaptoHold = false;
                ReleaseHold();
            }
            else
            {
                ReleaseTap();
            }
            yrot = 0;
            ydelrot = 0;
            if (!alwaysmove) shouldmove = false;
        }
        else
        {
            timeslowed -= Time.unscaledDeltaTime;
            if (timeslowed < 0)
            {
                timeslowed = 0;
            }

            if (movevalue < inputlimitcenter - inputlimit)
            {
                transform.position = Vector3.right * (inputlimitcenter - inputlimit);
            }
            else if (movevalue > inputlimitcenter + inputlimit)
            {
                transform.position = Vector3.right * (inputlimitcenter + inputlimit);
            }
        }

        if (!gameover)
        {
            if (inpool&&scorepertick!=0)
            {
                tickstartime += Time.deltaTime;
                while (tickstartime > ticktime)
                {
                    tickstartime -= ticktime;
                    agecontrol(scorepertick);
                }
            }
        }

        timer += Time.deltaTime;

        if(!gameover)timertext.text = timer.ToString();
    }
    void ReleaseHold()
    {
        // VERY EXPENSIVE. ONLY FOR PROTOTYPING
        //player.GetComponent<PlayerMover>().FaceForward();
    }
    void ReleaseTap()
    {
    }
    void Holding()
    {
        // VERY EXPENSIVE. ONLY FOR PROTOTYPING
        //player.GetComponent<PlayerMover>().Orienting(yrot/Screen.width);
    }
    float initangle;
    Vector3 initlocalcampos;
    float initdistance;
    public int camsteps = 100;

    public float levelwin()
    {
        gamend?.Invoke();
        camposindex = 1;
        float endist = agescore * endistscale;
        float endtime = endist / tempspeed;
        StartCoroutine(endflight(endtime));
        return endist;
    }

    public void levelfail()
    {
        gameover = true;
        diedingame?.Invoke();
        foreach (Animator anim in animators)
        {
            anim.SetTrigger("die");
        }
        UICtrl.instance.generateend(false, agescore, coinscore);
        SceneLoadController.instance.LevelFailed();
    }

    public bool Gated(Vector2 values, bool analog, bool selectgender)
    {
        float val = analog? Mathf.Lerp(values.x,values.y,(movevalue+inputlimit)/(2*inputlimit)) : (movevalue > 0 ? values.y : values.x);

        agecontrol(val);

        if (selectgender)
        {
            neutral.SetActive(false);
            (movevalue > 0?maleparticle:dressparticle).Play();
            male.SetActive(movevalue > 0);
            female.SetActive(movevalue <= 0);
        }

        return movevalue>0;
    }

    public void picked(float value,bool iscoin)
    {
        if (iscoin) coinctrl(value); else agecontrol(value);
    }

    void agecontrol( float months)
    {
        agescore += months;

        feedback( months);

        scoretext.text = " die after = " + ((int)(agescore / 12)).ToString() + " Yrs " + ((int)(agescore % 12)).ToString() + " Mnths, coin = " + coinscore.ToString();
    }

    void coinctrl(float coins)
    {
        coinscore += coins;

        currentcoin += coins;

        cointext.text = coinscore.ToString("F0");

        scoretext.text = " die after = " + ((int)(agescore / 12)).ToString() + " Yrs " + ((int)(agescore % 12)).ToString() + " Mnths, coin = " + coinscore.ToString();
    }

    float scorepertick;
    float ticktime;
    bool inpool;
    float tickstartime;

    public void enteredpool(float lefty, float righty, float tick)
    {
        bool rightside = movevalue > 0;
        scorepertick = rightside ? righty : lefty;
        ticktime = tick;
        inpool = true;
        tickstartime = 0;

        inputlimit = inputlimitmax / 2;
        inputlimitcenter = inputlimit * (rightside ? 1 : -1);
        inputlimit -= 0.1f;
    }

    public void exitedpool()
    {
        scorepertick = 0;
        ticktime = 0;
        inpool = false;
        tickstartime = 0;

        inputlimit = inputlimitmax;
        inputlimitcenter = 0;
    }

    public void limitmovementtemporarily(float tm)
    {
        StartCoroutine(limitmovement(tm));
    }

    IEnumerator limitmovement(float time)
    {
        inputlimit = 0.5f * inputlimitmax;
        yield return new WaitForSeconds(time);
        inputlimit = inputlimitmax;
    }

    IEnumerator endflight(float time)
    {
        foreach (Animator anim in animators)
        {
            anim.SetTrigger("fly");
        }

        float t = 0;
        
        while (t < time)
        {
            float norm = t / time;

            inputlimit = inputlimitmax * endanim.Evaluate(norm);

            t += Time.deltaTime;
            yield return null;
        }

        foreach (Animator anim in animators)
        {
            anim.SetTrigger("end");
        }

        inputlimit = 0;
        gameover = true;

        transform.position = Vector3.zero;
        camposindex = 2;
        Instantiate(coin, coinstart.position, coinstart.rotation);
        multipliertext.gameObject.SetActive(false);
        coinctrl(currentcoin * currentmult);
        gamefinished?.Invoke();
        UICtrl.instance.generateend(true, agescore, coinscore);
        SceneLoadController.instance.LevelFinished();
    }
    //Coroutine txtmov;
    public void feedback(float months)
    {
        bool positive = months > 0;
        string fbtxt = (positive ? "+" : "") + (months / 12).ToString("F1") + "Yrs";
        feedbacktext.text = fbtxt;
        //if(txtmov!=null)StopCoroutine(txtmov);
        StartCoroutine(textup(uptime, updist,positive,fbtxt));
    }

    IEnumerator textup(float time, float distance, bool pos, string txt)
    {
        TMPro.TMP_Text instantiatedtext = Instantiate(feedbacktext.gameObject, feedbacktext.transform.parent).GetComponent<TMPro.TMP_Text>();
        instantiatedtext.gameObject.SetActive(true);
        instantiatedtext.rectTransform.anchoredPosition = initposfb;
        Color col = pos ? positivefdback : negativefdback;
        instantiatedtext.color = col;
        float t = 0;
        while (t < time)
        {
            float norm = t / time;

            instantiatedtext.rectTransform.anchoredPosition =initposfb + Vector2.up * norm * distance;
            instantiatedtext.color = new Color(col.r, col.g, col.b, (1 - norm));


            t += Time.deltaTime;
            yield return null;
        }

        instantiatedtext.rectTransform.anchoredPosition = initposfb + Vector2.up  * distance;
        instantiatedtext.color = new Color(col.r, col.g, col.b, 0);

        instantiatedtext.gameObject.SetActive(false);

        Destroy(instantiatedtext.gameObject);
    }
}
