using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class coincollectible : MonoBehaviour
{
    public Transform rotfollow;
    public Transform follow;
    public float followlerprate = 1;
    public float collectionradius = 1;
    public string debug;

    // Start is called before the first frame update
    void Start()
    {
        follow = PlayerController.instance.coinhome;
    }

    // Update is called once per frame
    void Update()
    {
        //transform.rotation = rotfollow.rotation;
        transform.position = Vector3.Lerp(transform.position, follow.position, Time.deltaTime * followlerprate);
        transform.localScale = Vector3.Lerp(transform.localScale, follow.localScale, Time.deltaTime * followlerprate);

        if ((transform.position - follow.position).sqrMagnitude < collectionradius * collectionradius) { Destroy(gameObject); } else debug = (transform.position - follow.position).sqrMagnitude.ToString();
    }


}
