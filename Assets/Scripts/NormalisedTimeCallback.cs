﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class NormalisedTimeCallback : StateMachineBehaviour
{
    public List<TimelineHolder> timelines;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        foreach (TimelineHolder tl in timelines)
        {
            tl.passed = false;

        }
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        foreach (TimelineHolder tl in timelines)
        {
            if (stateInfo.normalizedTime > tl.normalizedtime)
            {
                if (!tl.passed)
                {
                    tl.passed = true;
                    tl.callback?.Invoke();
                }
            }

        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}


    [System.Serializable]
    public class TimelineHolder
    {
        public bool passed;
        [Range(0,1)]public float normalizedtime;
        public UnityEvent callback;

        public TimelineHolder(float normaltime, UnityAction action=null)
        {
            passed = false;
            normalizedtime = normaltime;
            callback = new UnityEvent();
            if (action!=null) callback.AddListener(action);
        }
    }

}
