using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactible : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        onStart();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public virtual void onInteraction() { }
    public virtual void onExit() { }
    public virtual void onStart() { }

    private void OnTriggerEnter(Collider other)
    {
        onInteraction();
    }

    private void OnTriggerExit(Collider other)
    {
        onExit();
    }
}
