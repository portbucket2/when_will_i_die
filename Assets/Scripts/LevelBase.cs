using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelBase : MonoBehaviour
{
    public float turnsped = 10f;
    public float movespeed = 5f;
    public designinterface movespeedinterface;
    public designinterface turnspeedinterface;
    public Transform prevforward;
    public Transform endpoint;
    [TextArea] public string debug;
    // Start is called before the first frame update
    void Start()
    {
        PlayerController.instance.tempspeed = movespeedinterface.designvalue;
        PlayerController.instance.gamefinished += generateend;
    }

    // Update is called once per frame
    void Update()
    {
        if(!PlayerController.instance.gameover && PlayerController.instance.shouldmove)transform.position -= movespeedinterface.designvalue * Time.deltaTime * Vector3.forward;

        float moveangle = turnspeedinterface.designvalue * Time.deltaTime;

        //Vector2 vecpos = new Vector2(prevforward.position.x, prevforward.position.z);
        //Vector2 vecfor = new Vector2(prevforward.forward.x, prevforward.forward.z);

        float pnt = prevforward.position.z - (prevforward.position.x * (prevforward.forward.z / prevforward.forward.x));

        debug = "center rot = " + pnt;
        float angle = Vector3.SignedAngle(Vector3.forward, new Vector3(prevforward.forward.x, 0, prevforward.forward.z), Vector3.up);

        if (prevforward.forward.x == 0)
        {
            pnt = 0;angle = 0;
        }

        moveangle = Mathf.Abs(angle) > moveangle ? Mathf.Sign(angle) * moveangle : angle;

        transform.RotateAround(Vector3.forward * pnt, Vector3.up, -moveangle);

        debug = "center rot = " + Vector3.forward * pnt + "\n angle = " + angle + "\n moveangle = " + moveangle;
    }

    public void turn(Transform ornt)
    {
        prevforward = ornt;
        PlayerController.instance.limitmovementtemporarily(90 / turnspeedinterface.designvalue);
    }

    public void generateend()
    {
        float dist = transform.InverseTransformPoint(endpoint.position).z;
        //transform.position = -dist* Vector3.forward;

        transform.position = endpoint.InverseTransformPoint(transform.position);
        transform.rotation *= Quaternion.Inverse(endpoint.rotation);
    }

    //IEnumerator changedir(Transform changedforward)
    //{

    //    //float t = 0;
    //    //while (t < turntime)
    //    //{
    //    //    float norm = t / turntime;

    //    //    t += Time.deltaTime;
    //    //    yield return null;
    //    //}
    //}
}
