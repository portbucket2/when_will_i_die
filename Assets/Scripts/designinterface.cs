using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class designinterface : MonoBehaviour
{
    public float basevalue;

    InputField input;

    public float designvalue
    {
        get
        {
            return PlayerPrefs.GetFloat(name, basevalue);
        }

        set
        {
            PlayerPrefs.SetFloat(name, value);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        input = GetComponent<InputField>();
        input.text = designvalue.ToString();
        input.onEndEdit.AddListener((val) => designvalue = float.Parse(val));
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}
