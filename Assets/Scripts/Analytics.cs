using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LionStudios.Suite.Analytics;
using GameAnalyticsSDK;
using ByteBrewSDK;

public class Analytics : MonoBehaviour
{
    public static Analytics instance;
    int levelnumber
    {
        get { return PlayerPrefs.GetInt("ANALYTICS_LEVEL_NUM", 1); }
        set { PlayerPrefs.SetInt("ANALYTICS_LEVEL_NUM", value); }

    }
    int attemptnumber
    {
        get { return PlayerPrefs.GetInt("ANALYTICS_ATTEMPT_NUM", 1); }
        set { PlayerPrefs.SetInt("ANALYTICS_ATTEMPT_NUM", value); }

    }

    int shoptracking
    {
        get { return PlayerPrefs.GetInt("SHOP_TRACKING_NUM", 1); }
        set { PlayerPrefs.SetInt("SHOP_TRACKING_NUM", value); }

    }

    int towncentertracking
    {
        get { return PlayerPrefs.GetInt("TOWNCENTER_TRACKING_NUM", 1); }
        set { PlayerPrefs.SetInt("TOWNCENTER_TRACKING_NUM", value); }

    }
    int upgradecentertracking
    {
        get { return PlayerPrefs.GetInt("UPGRADECENTER_TRACKING_NUM", 1); }
        set { PlayerPrefs.SetInt("UPGRADECENTER_TRACKING_NUM", value); }

    }
    int sawmilltracking
    {
        get { return PlayerPrefs.GetInt("SAWMILL_TRACKING_NUM", 1); }
        set { PlayerPrefs.SetInt("SAWMILL_TRACKING_NUM", value); }

    }
    int farmtracking
    {
        get { return PlayerPrefs.GetInt("FARM_TRACKING_NUM", 1); }
        set { PlayerPrefs.SetInt("FARM_TRACKING_NUM", value); }

    }
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        ByteBrew.InitializeByteBrew();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void levelwin(int level)
    {
        LionAnalytics.LevelComplete(level, attemptnumber);
        ByteBrew.NewProgressionEvent(ByteBrewProgressionTypes.Completed, "LEVEL", level.ToString());
        attemptnumber = 1; 
    }



    public void levelfailed(int level)
    {
        LionAnalytics.LevelFail(level, attemptnumber++);
        ByteBrew.NewProgressionEvent(ByteBrewProgressionTypes.Failed, "LEVEL", level.ToString());
        //GameAnalytics.NewDesignEvent("LEVEL_FAILED:"+failindex.ToString(), failindex);
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, failindex.ToString());
    }

    public void levelstart(int level)
    {
        LionAnalytics.LevelStart(level, attemptnumber);
        ByteBrew.NewProgressionEvent(ByteBrewProgressionTypes.Started, "LEVEL", level.ToString());
        //GameAnalytics.NewDesignEvent("LEVEL_FAILED:"+failindex.ToString(), failindex);
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, failindex.ToString());
    }

    public void levelrestart(int level)
    {
        LionAnalytics.LevelRestart(level, attemptnumber);
        GameAnalytics.NewDesignEvent("LEVEL_RESTART", level);
        ByteBrew.NewCustomEvent("LEVEL_RESTARTED", level.ToString());
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, failindex.ToString());
    }




}
