using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : Interactible
{
    public float leftvalue = 100;
    public float rightvalue = -100;
    public bool analog;
    public bool gender;

    public GameObject Gateleftpos, Gateleftneg, Gaterightpos, Gaterightneg;

    public GameObject left;
    public GameObject right;
    public GameObject leftpos, leftneg, rightpos, rightneg;
    public GameObject textcanvas;

    public static List<Gate> allgates;

    public float playerdistcheck;
    public override void onStart()
    {
        if (allgates == null) allgates = new List<Gate>();
        allgates.Add(this);
        PlayerController.instance.highscoretracker += leftvalue > rightvalue ? leftvalue : rightvalue;

        Gateleftpos.SetActive(leftvalue > 0);
        Gateleftneg.SetActive(leftvalue <= 0);
        Gaterightpos.SetActive(rightvalue > 0);
        Gaterightneg.SetActive(rightvalue <= 0);
    }

    // Update is called once per frame
    void Update()
    {
        playerdistcheck = (transform.position - PlayerController.instance.transform.position).z;

        bool b = true;
        foreach (var item in allgates)
        {
            if (item.playerdistcheck > 0)
            {
                b = b && playerdistcheck>0 && item.playerdistcheck >= playerdistcheck;
            }
        }

        textcanvas.SetActive(b);
    }

    public override void onInteraction()
    {
        bool rt = PlayerController.instance.Gated(new Vector2(leftvalue,rightvalue),analog,gender);

        if (!analog)
        {
            left.SetActive(rt);
            right.SetActive(!rt);
            leftpos.SetActive(!rt && leftvalue>0);
            leftneg.SetActive(!rt && leftvalue <= 0);
            rightpos.SetActive(rt && rightvalue > 0);
            rightneg.SetActive(rt && rightvalue <= 0);

        }
    }
}
