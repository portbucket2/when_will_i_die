using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sliderdesigninteface : designinterface
{
    Slider inputslider;

    // Start is called before the first frame update
    void Start()
    {
        inputslider = GetComponent<Slider>();
        inputslider.value = designvalue;
        inputslider.onValueChanged.AddListener((val) => designvalue = val);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
