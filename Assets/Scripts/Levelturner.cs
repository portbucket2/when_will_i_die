using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Levelturner : Interactible
{
    [SerializeField]RoadBase road;
    

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void onInteraction()
    {
        road.enteredroad();
    }

    public override void onStart()
    {
        road = GetComponentInParent<RoadBase>();
    }

}
