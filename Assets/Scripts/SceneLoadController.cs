﻿using System.Collections;
using System.Collections.Generic;
//using FRIA;
using UnityEngine;
using UnityEngine.SceneManagement;
//using Facebook.Unity;
using PotatoSDK;


public class SceneLoadController : MonoBehaviour
{
    public static SceneLoadController instance;

    public int numlevels = 4;

    public int LevelData
    {
        get { return PlayerPrefs.GetInt("LEVEL_INDEX", 0); }
        set { PlayerPrefs.SetInt("LEVEL_INDEX", value);  }
    }

    // Start is called before the first frame update
    IEnumerator Start()
    {
        DontDestroyOnLoad(gameObject);


        if (!instance)
        {
            instance = this;
            //yield return new WaitForSeconds(0.5f);
            while (!Potato.IsReady)
            {
                yield return null;
            }

            AdOnCooldown = true;
            Invoke("CooledDown", AdFreq);

            LionStudios.Suite.Analytics.LionAnalytics.GameStart();

            LevelStart();
        }
        else
        {
            Destroy(gameObject);
        }

        
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.Space)) LevelFinished();
    }

    

    public void LevelFinished()
    {

        //AnalyticsControllerOriginal.LogEvent_LevelCompleted(LevelData + 1);
        Debug.Log("next level called by " + (((LevelData) % numlevels) + 1).ToString());
        Analytics.instance.levelwin(LevelData + 1);
        LevelData++;

        //Invoke("loadlevel", delay);
        //loadlevel();
    }

    public void LevelRestart()
    {
        
        //AnalyticsControllerOriginal.LogEvent_LevelRestarted(LevelData + 1);
        Debug.Log("restart level called by " + (((LevelData) ) + 1).ToString());
        Analytics.instance.levelrestart(LevelData + 1);
        //Invoke("loadlevel",delay);
        LevelStart();
    }

    public void LevelStart()
    {

        //AnalyticsControllerOriginal.LogEvent_LevelRestarted(LevelData + 1);
        Debug.Log("start level called by " + (((LevelData)) + 1).ToString());
        Analytics.instance.levelstart(LevelData + 1);
        //Invoke("loadlevel",delay);
        loadlevel();
    }

    public void LevelFailed()
    {

        //AnalyticsControllerOriginal.LogEvent_LevelFailed(LevelData + 1);
        Analytics.instance.levelfailed(LevelData + 1);
    }

    void loadlevel()
    {
        InterstitialAdShowRequest();

        //AnalyticsControllerOriginal.LogEvent_LevelStarted(leveldata.value + 1);
        Debug.Log("start level called by " + (((LevelData)) + 1).ToString());
        SceneManager.LoadScene(((LevelData) % numlevels) +1);
        
    }

    public float AdFreq;

    bool AdOnCooldown;

    public float delay = 2;

    public void InterstitialAdShowRequest()
    {
        if (!AdOnCooldown)
        {
            AdOnCooldown = true;

            ShowIntertitialAd();
            Invoke("CooledDown", AdFreq);
        }
    }

    void ShowIntertitialAd()
    {
        //LionStudios.Ads.Interstitial.Show();
    }

    void CooledDown()
    {
        AdOnCooldown = false;
    }

    //bool adshowgui = false;

    //void OnGUI()
    //{
    //    if(AdOnCooldown)GUI.Label(new Rect(10, 10, 100, 20), "Ad Shown!");
    //}
}
