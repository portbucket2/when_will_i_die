using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UICtrl : MonoBehaviour
{
    public static UICtrl instance;
    public GameObject winpanel, failpanel;
    public Button next, retry;
    public TMP_Text windescrpt, coincount;
    public GameObject[] deactives;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    string generatetext(float month)
    {
        System.DateTime date = System.DateTime.Today.AddDays(Random.Range(0,365)).AddSeconds(Random.Range(0, 24 * 60 * 60)).AddMonths((int)month);
        //System.DateTime time = System.DateTime.Now.AddSeconds(Random.Range(0,24*60*60));

        return date.ToString("hh:mm tt\nd MMMM\nyyyy");
    }

    public void generateend(bool win, float months, float coins)
    {
        coincount.text = coins.ToString("F0");
        windescrpt.text = generatetext(months);
        next.onClick.AddListener(SceneLoadController.instance.LevelStart);
        retry.onClick.AddListener(SceneLoadController.instance.LevelRestart);
        winpanel.SetActive(win);
        failpanel.SetActive(!win);
        if(win)StartCoroutine(sequentialactivation());
    }

    IEnumerator sequentialactivation()
    {
        int n = 0;
        while (n < deactives.Length)
        {
            deactives[n++].SetActive(true);
            yield return new WaitForSeconds(0.25f);
        }
    }
}
