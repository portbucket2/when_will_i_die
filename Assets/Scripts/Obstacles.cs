using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacles : Interactible
{
    public bool moving;
    public bool up;

    public float movefreq=1;
    public float moveamplitude=1;

    bool killed;

    Vector3 initpos;
    // Start is called before the first frame update
    void Start()
    {
        initpos = transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
        float dist = moveamplitude * Mathf.Sin(2 * Mathf.PI * movefreq * Time.time);

        if(!killed)transform.localPosition = initpos + (moving ? (up ? Vector3.forward : Vector3.right) : (up ? Vector3.up : Vector3.zero)) * dist;
    }

    public override void onInteraction()
    {
        GetComponentInChildren<Animator>().SetTrigger("slash");
        PlayerController.instance.levelfail();
        killed = true;
    }
}
