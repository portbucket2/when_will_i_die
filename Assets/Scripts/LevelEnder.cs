using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEnder : Interactible
{
    public Transform gateL;
    public Transform gateR;
    public float opentime = 0.5f;
    public float flowertime = 0.5f;
    public float flowerdist = 5f;
    public float graveoffset = -0.15f;
    public float gravespawnoffset = 10f;

    public Transform grave;
    public Transform bouqet;

    public GameObject multiplier;
    public GameObject coin;
    public override void onInteraction()
    {
        
        StartCoroutine(opengate(PlayerController.instance.levelwin()));
    }

    public override void onStart()
    {
        PlayerController.instance.gamefinished+=endgame;
    }

    IEnumerator opengate(float gravedist)
    {
        float maxdist = PlayerController.instance.highscoretracker * PlayerController.instance.endistscale;
        for (int i = 0; i < 10; i++)
        {
            float thisdist = maxdist * (i + 1f) / 11f;
            if ((thisdist+gravespawnoffset) < gravedist)
            {
                int multspot = Random.Range(-1, 2);
                for (int j = -1; j < 2; j++)
                {
                    if (j == multspot)
                    {
                        Instantiate(multiplier, transform.position + Vector3.forward * thisdist + Vector3.right * 2 * j + Vector3.up, multiplier.transform.rotation, transform);
                    }
                    else if(Random.Range(0,2)>0)
                    {
                        Instantiate(coin, transform.position + Vector3.forward * thisdist + Vector3.right * 2 * j + Vector3.up, coin.transform.rotation, transform);
                    }
                }
                
            }
        }
        grave.position += Vector3.forward * (gravedist+graveoffset);
        grave.gameObject.SetActive(true);
        float t = 0;
        while (t < opentime)
        {
            float angle = 90 * t / opentime;
            gateR.eulerAngles = Vector3.up * angle;
            gateL.eulerAngles = Vector3.down * angle;
            t += Time.deltaTime;
            yield return null;
        }

        gateR.eulerAngles = Vector3.up * 90;
        gateL.eulerAngles = Vector3.down * 90;
    }

    public void endgame()
    {
        StartCoroutine(flowered());
    }

    IEnumerator flowered()
    {
        bouqet.gameObject.SetActive(true);
        Vector3 flowerpos = bouqet.localPosition;
        float t = 0;
        while (t < flowertime)
        {
            float norm = t / flowertime;

            bouqet.localPosition = flowerpos + Vector3.up * (1-norm) * flowerdist;
            t += Time.deltaTime;
            yield return null;
        }

        bouqet.localPosition = flowerpos;
    }
}
