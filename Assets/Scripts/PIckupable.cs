using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PIckupable : Interactible
{
    public float value;
    public bool coin;
    public bool multiplier;
    public GameObject positive, negative, mltplr,coincollectible;
    public override void onStart()
    {
        PlayerController.instance.highscoretracker += value > 0 ? value : 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void onInteraction()
    {
        if (coin) Instantiate(coincollectible, transform.TransformPoint(coincollectible.transform.localPosition), coincollectible.transform.rotation);
        Instantiate((coin || multiplier) ? mltplr : (value > 0 ? positive : negative), transform.position + Vector3.up, Quaternion.identity,transform.parent);
        if (!multiplier)
        {
            PlayerController.instance.picked(value, coin);
            
        }
        else
        {
            PlayerController.instance.currentmult++;
            PlayerController.instance.multipliertext.gameObject.SetActive(true);
            PlayerController.instance.multipliertext.text="x"+((int)PlayerController.instance.currentmult).ToString();
        }
        Destroy(gameObject);
    }
}
