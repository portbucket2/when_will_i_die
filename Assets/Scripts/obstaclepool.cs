using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class obstaclepool : Interactible
{
    public float scoreleftpertick;
    public float scorerightpertick;
    public float ticklength=0.5f;
    // Start is called before the first frame update
    public override void onInteraction()
    {
        PlayerController.instance.enteredpool(scoreleftpertick,scorerightpertick,ticklength);
    }

    public override void onExit()
    {
        PlayerController.instance.exitedpool();
    }


}
